
/*
import {qase} from 'playwright-qase-reporter';
// @ts-check
const { test, expect } = require('@playwright/test');

test('has title', async ({ page }) => {
  qase.id(11);
  await page.goto('https://playwright.dev/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);
});

test('get started link', async ({ page }) => {
  qase.id(1);
  await page.goto('https://playwright.dev/');

  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();

  // Expects page to have a heading with the name of Installation.
  await expect(page.getByRole('heading', { name: 'Installation' })).toBeVisible();
});

*/

import { qase } from 'playwright-qase-reporter';
import { test , expect } from '@playwright/test';

  test('Simple test', () => {
    qase.id(1);
    qase.title('Example of simple test');
    expect(true).toBe(true);
  });


  test('Test with steps', async () => {
    await test.step('Step 1', async () => {
      expect(false).toBe(true);
    });
    await test.step('Step 2', async () => {
      expect(true).toBe(true);
    });
    expect(true).toBe(true);
  });

